import './actions';
import './socket';
import io from 'socket.io-client';
let currentPosition = {x:0 ,y:1,z:0 };
let socket = io.connect('http://' + location.host);
let me;

function drawOpponent(data){
    let box = document.createElement("a-box");
    box.setAttribute('id',"p"+data.id);
    box.setAttribute('position', data.spawn);
    box.setAttribute('depth', 0.5);
    box.setAttribute('height', 1.5);
    box.setAttribute('width', 0.8);

    if(data.faction === 0)
        box.setAttribute('color', '#ff0000');
    else if(data.faction === 1)
        box.setAttribute('color', '#0000ff');
    else if(data.faction === 2)
        box.setAttribute('color', '#00ff1c');

    data.spawn.y = data.spawn.y + 1.1;

    let head = document.createElement("a-box");
    head.setAttribute('color', '#3fe9ff');
    head.setAttribute('id',"ph"+data.id);
    head.setAttribute('position', data.spawn);
    head.setAttribute('depth', 0.44);
    head.setAttribute('height', 0.44);
    head.setAttribute('width', 0.44);


    document.getElementById("scene").appendChild(box);
    document.getElementById("scene").appendChild(head);
}

function drawWall(data){
    let wall = document.createElement("a-box");
    wall.setAttribute('id',"w"+data.id);
    wall.setAttribute('position', data.position);
    wall.setAttribute('height', 2);
    wall.setAttribute('width', 10);
    wall.setAttribute('color', "#FFFFFF");
    wall.setAttribute('rotation', data.rotation);
    wall.setAttribute('static-body', true);

    document.getElementById("scene").appendChild(wall);
}

window.addEventListener('DOMContentLoaded',()=>{

    document.querySelector('#camera').addEventListener('componentchanged', function (evt) {
        if (evt.detail.name === 'position') {
            let world = evt.detail.newData;

            if(world.x.toFixed(1) > currentPosition.x ||
                world.x.toFixed(1) < currentPosition.x ||
                world.z.toFixed(1) > currentPosition.z ||
                world.z.toFixed(1) < currentPosition.z
            ){
                currentPosition.x=world.x.toFixed(1);
                currentPosition.z = world.z.toFixed(1);
                notifyMovement(currentPosition);
            }
        }
        else if(evt.detail.name === 'rotation'){
            notifyMovement(currentPosition);
        }




    });

    document.body.onkeydown = function(e){
        if(e.keyCode === 32){

            let rotation = document.querySelector('#camera').getAttribute("rotation");

            let request_data = {id: me.id, position: currentPosition, rotation: rotation};

            socket.emit("r_showWall",request_data);
        }
    };

    document.querySelector('#flag0').addEventListener('click', function (evt) {
        socket.emit("conquested",{id:0, faction: me.faction});
    });
    document.querySelector('#flag1').addEventListener('click', function (evt) {
        socket.emit("conquested",{id:1, faction: me.faction});
    });
    document.querySelector('#flag2').addEventListener('click', function (evt) {
        socket.emit("conquested",{id:2, faction: me.faction});
    });

    socket.on("wllOP",function (wall) {
        if(wall.build)
            drawWall(wall);
        else {
            console.log("wall delete");
            console.log(wall);
            document.querySelector('#w' + wall.id).outerHTML = "";
        }
    });

    socket.on('id_assigned', function (data) {
        //console.log("Id obtained, my id is: " + data.id);
        console.log( data);
        me = {id: data.id, faction: data.faction};
        document.querySelector('#camera').setAttribute('position',data.spawn);
        if(data.faction !== 0)
            document.querySelector('#camera').setAttribute('rotation',{x:0,y:180,z:0});

        //se ci sono altri giocatori li disegno sulla mappa
        console.log(data.players);
        for(let i = 0;i<data.players.length;i++){
            drawOpponent(data.players[i]);
        }
        for(let i = 0;i<data.flags.length;i++) {
            if(data.flags[i]!==-1){
                if(data.flags[i] === 0)
                    document.querySelector('#flag' + i).setAttribute('color', '#ff0000');
                else if(data.flags[i] === 1)
                    document.querySelector('#flag'+ i).setAttribute('color', '#0000ff');
                else if(data.flags[i] === 2)
                    document.querySelector('#flag'+ i).setAttribute('color', '#00ff1c');
            }
        }
    });

    socket.on('new_player', function (data) {
        console.log("new Player detected id: " + data.id);
        drawOpponent(data);

    });

    socket.on('player_exit', function (data) {
        console.log("Player disconnected id: " + data.id);

        let element = document.querySelector('#p'+data.id);
        element.outerHTML = "";

        let elementh = document.querySelector('#ph'+data.id);
        elementh.outerHTML = "";

    });

    socket.on('player_moved', function (data) {
        let player = document.querySelector("#p"+data.id);
        let player_head = document.querySelector("#ph"+data.id);

        player.setAttribute('position',data.position);

        data.position.y = data.position.y + 1.1;

        player_head.setAttribute('position',data.position);
        player_head.setAttribute('rotation',data.position.rotation);

        data.position.rotation.z =0;
        data.position.rotation.x=0;

        player.setAttribute('rotation',data.position.rotation);


    });

    socket.on('start', function (isStarted) {
        console.log("Game started!!!");

        let i = 3;

        if(isStarted){
            writeToScreen("");
            document.querySelector('#camera').setAttribute('universal-controls',true);
            document.querySelector('#camera').setAttribute('kinematic-body',true);
        }
        else {

            let inter = setInterval(function () {


                if (i === 0)
                    writeToScreen("GOGOGOGOGOGO!!!")

                else
                    writeToScreen(i);

                if (i === -1) {
                    clearInterval(inter);

                    document.querySelector('#camera').setAttribute('universal-controls', true);
                    document.querySelector('#camera').setAttribute('kinematic-body', true);

                    writeToScreen("");
                }

                i--;

            }, 1000);
        }


    });

    socket.on('game_over', function (data) {
        console.log("Game over!!!");
        document.querySelector('#camera').removeAttribute('universal-controls');
        document.querySelector('#camera').removeAttribute('kinematic-body');
        document.querySelector('#camera').setAttribute('position', {x:0,y:30,z:0});
        document.querySelector('#camera').setAttribute('rotation', {x:-90,y:0,z:0});
        let teams = ["red", "blue", "green"];
        writeToScreen(teams[data.faction] + " win");
    });

    function notifyMovement(position){

        position.rotation = document.querySelector('#camera').getAttribute("rotation");

        socket.emit("i_moved",position);
    }

    socket.on('flag_conquested', function (data) {
        if(data.faction === 0)
            document.querySelector('#flag' +data.id).setAttribute('color', '#ff0000');
        else if(data.faction === 1)
            document.querySelector('#flag'+data.id).setAttribute('color', '#0000ff');
        else if(data.faction === 2)
            document.querySelector('#flag'+data.id).setAttribute('color', '#00ff1c');
    });


    function writeToScreen(text){
        document.querySelector('#userText')
            .setAttribute("text","width: .4; align:center; value:"+text);
    }

});
