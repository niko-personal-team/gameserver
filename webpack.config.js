const path = require('path');
const webpack = require('webpack');

module.exports = {
  // devtool: 'inline-source-map',
   devtool: 'source-map',
  entry: {
      aframe: './src/js/aframe.js',
      flag: './src/js/main.js',
      proxy: './node_modules/aframe-proxy-controls/dist/aframe-proxy-controls.min.js'
  },
  output: {
    path: path.join(__dirname, "www/dist"),
    filename: '[name]-bundle.js'
  },
  resolve: {
    alias: {
      openlayers$: path.resolve(__dirname, 'node_modules/openlayers/build/ol-paradox.js')
    },
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: ['.ts', '.tsx', '.js'] // note if using webpack 1 you'd also need a '' in the array as well
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      }
    ]
        // loaders: [ // loaders will work with webpack 1 or 2; but will be renamed "rules" in future
    //   // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
    //   { 
    //     test: /\.tsx?$/,
    //     loader: 'ts-loader', 
    //     exclude: /\.spec.ts$/
    //   }
    //
  }
  //,
  //plugins: [
  //  new webpack.optimize.UglifyJsPlugin()
  //]
};
