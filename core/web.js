const express = require('express');
const app = express();
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

const songs = require('j5-songs');

const min_players = 1;

//reset lampade
lampOn(0,false);
lampOn(1,false);
lampOn(2,false);

let five = require("johnny-five"),
    board, lcd, piezo;

board = new five.Board();


let players = [];

let gameStarted = false;

let factions = [0,0,0];
let factions_name = ["RED","BLU","GREEN"];
//0 => rosso
//1 => blu
//2 => verde

let flags = [-1,-1,-1];

const factions_spawn = [{x:-18,y:5,z:18}, {x:18,y:5,z:-18}, {x:-18,y:5,z:-18}];

let walls = [];




board.on("ready", function() {


    let ledRGB = new five.Led.RGB({
        pins: {
            red: 3,
            green: 5,
            blue: 4
        }
    });



    piezo = new five.Piezo(6);

    lcd = new five.LCD({
        // LCD pin name  RS  EN  DB4 DB5 DB6 DB7
        // Arduino pin # 7    8   9   10  11  12
        pins: [7, 8, 9, 10, 11, 12],
        backlight: 6,
        rows: 2,
        cols: 20
    });

    lcd.cursor(0,0);
    lcd.print("Waiting players");

    this.repl.inject({
        lcd: lcd
    });


app.use(express.static('www'));
let server = app.listen(4000, () => {
  const address = server.address();
  console.log(`IoT capture the flag game server listening at http://${address.address}:${address.port}`);
  //ws.init(core);
});

    const io = require('socket.io')(server);


setInterval(function(){

    for(let i=0;i<walls.length;i++)
        if(walls[i].expiration < Date.now() && walls[i].active) {
            for (let y = 0; y < players.length; y++) {
                players[y].socket.emit("wllOP", {id: walls[i].id, build: false});
                console.log("wall expired");
            }
            walls[i].active=false;
        }

}, 1000);


io.on('connection', function (socket) {

    //id è la lunghezza della lista
    let player_id = Date.now();

    socket.id = player_id;

    //assegno la fazione
    let fac = getMinFaction(factions);

    //dico al client il suo id, il tipo di fazione e lo spawnpoint e tutti gli altri giocatori

    player = {id:player_id, faction: fac, socket: socket, spawn: factions_spawn[fac], wallReactive:Date.now()};


    others = [];
    for(let i=0;i<players.length;i++)
        others.push({id:players[i].id, spawn:players[i].spawn,faction:players[i].faction});

    socket.emit("id_assigned",{ id: player_id, faction: fac, spawn: factions_spawn[fac], players: others, flags:flags});

    console.log("New player connected and id is assigned: " + player_id);


    //mando in broadcast il nuovo giocatore a tutti gli altri.
    for(let i = 0;i<players.length;i++)
        players[i].socket.emit("new_player",{id: player_id, faction: fac, spawn: factions_spawn[fac]});

    //aggiungo il socket alla lista
    players.push(player);
    factions[fac]++;
    if(players.length>=min_players)
        startGame(socket);


    //nel caso che un giocatore si scollega gestisco la disconnessione
    socket.on('disconnect', function () {
        let player;
        for(let i = 0;i<players.length;i++)
            if(players[i].id === socket.id){
                player = players[i];
                break;
            }

        console.log("Player disconnected: " + player.id);
        //elimino il socket dalla lista
        players.splice(players.indexOf(player), 1);


        console.log("total Players " + players.length);

        //mando in broadcast di eliminare il giocatore
        for(i = 0;i<players.length;i++)
            players[i].socket.emit("player_exit",{id: player.id});

    });

    socket.on('i_moved', function (position) {
        //console.log("remote player moved in x: " + position.x  + " z: " + position.z);
        for(let i = 0;i<players.length;i++)
            if(socket.id!==players[i].id)
                players[i].socket.emit("player_moved",{id: player_id, position: position});
    });

    socket.on('conquested', function (data) {

        if(data.faction === 0) {
            lampColor(data.id, 65535);
            writeEvent("RED SCORED!");
            piezo.frequency(1000, 200);
        }
        else if(data.faction === 1) {
            lampColor(data.id, 43690);
            writeEvent("BLUE SCORED!");
            piezo.frequency(1000, 300);
        }
        else if(data.faction === 2) {
            lampColor(data.id, 21845);
            writeEvent("GREEN SCORED!");
            piezo.frequency(1000, 400);
        }

        console.log('cambiocolore');

        for(let i = 0;i<players.length;i++) {
            players[i].socket.emit("flag_conquested", {id: data.id, faction: data.faction});
            flags[data.id] = data.faction;
        }

        //if(arduinoRead)
        //ledsScore();


        if(flags[0] === flags[1] && flags[0] === flags[2]) {
            for (let i = 0; i < players.length; i++) {
                players[i].socket.emit("game_over", {faction: flags[1]});

            }

            console.log("GAME OVER! " + flags[1] + " WON!");


            lcd.clear();
            lcd.cursor(0,0);
            lcd.print("Game Over!");
            lcd.cursor(1,0);
            lcd.print(factions_name[flags[1]] + " WIN!!");

            piezo.play({
                // song is composed by an array of pairs of notes and beats
                // The first argument is the note (null means "no note")
                // The second argument is the length of time (beat) of the note (or non-note)
                song: [
                    ["C4", 1 / 4],
                    ["D4", 1 / 4],
                    ["F4", 1 / 4],
                    ["D4", 1 / 4],
                    ["A4", 1 / 4],
                    [null, 1 / 4],
                    ["A4", 1],
                    ["G4", 1],
                    [null, 1 / 2],
                    ["C4", 1 / 4],
                    ["D4", 1 / 4],
                    ["F4", 1 / 4],
                    ["D4", 1 / 4],
                    ["G4", 1 / 4],
                    [null, 1 / 4],
                    ["G4", 1],
                    ["F4", 1],
                    [null, 1 / 2]
                ],
                tempo: 100
            });

            let counter = 0;

            if(flags[0] === 0)
                ledRGB.color("#FF0000");
            else if(flags[0] === 1)
                ledRGB.color("#0000ff");
            else if(flags[0] === 2)
                ledRGB.color("#00FF00");

            let inter = setInterval(function(){

                if (counter%2)
                {
                    ledRGB.on();
                    lampOn(0,true);
                    lampOn(1,true);
                    lampOn(2,true);
                }
                else
                {

                    ledRGB.off();
                    lampOn(0,false);
                    lampOn(1,false);
                    lampOn(2,false);

                }
                counter++;

                if(counter===10){
                    clearInterval(inter);
                }

            }, 1000);
        }
        else
            updateScoreLCD();

    });

    socket.on('r_showWall', function (data) {

        console.log("wall requested by " + data.id + " to: x" + data.position.x + " z:" + data.position.z);

        //todo: check se far disegnare il muro o meno
        for(let i=0;i<players.length;i++)
            if(players[i].id === data.id && players[i].wallReactive > Date.now())
                return;
            else if(players[i].id === data.id && players[i].wallReactive < Date.now())
                players[i].wallReactive= Date.now() + 10000;

        data.rotation.x = 0;
        data.rotation.z = 0;

        let rep = {id: data.id, position: data.position, build: true, rotation: data.rotation};
        for (let i = 0; i < players.length; i++)
            players[i].socket.emit("wllOP", rep);

        walls.push({id: data.id, expiration: Date.now() + 5000, active:true})
    });

});

function getMinFaction(temp){
    let index = 0;
    let value = temp[0];
    for (let i = 1; i < temp.length; i++) {
        if (temp[i] < value) {
            value = temp[i];
            index = i;
        }
    }
    return index;
}

function startGame(socket){
    if(gameStarted){
        socket.emit("start", true);
    }
    else{
        gameStarted = true;
        for(let i = 0;i<players.length;i++)
            players[i].socket.emit("start");

        lcd.clear();
        lcd.cursor(0,0);

        i = 3;

        let inter = setInterval(function () {

            lcd.clear();

            if (i === 0) {
                lcd.print("GOGOGOGOGOGO!!!");
                piezo.frequency(1200, 500);
            }
            else if (i === -1) {
                clearInterval(inter);
                updateScoreLCD();
            }
            else {
                lcd.print(i + "...");
                piezo.frequency(900, 200);
            }

            i--;

        }, 1000);



    }





}

});

function lampColor(id, color){
    let xhr = new XMLHttpRequest;
    xhr.open('GET', 'http://localhost:3000/devices/' + id + '/color/' + color);
    xhr.send(null);
}

function lampOn(id, action){
    let xhr = new XMLHttpRequest;
    xhr.open('GET', 'http://localhost:3000/devices/' + id + '/on/' + action);
    xhr.send(null);
}


function updateScoreLCD() {
    lcd.cursor(1,0);
    let points = [0,0,0];

    for(let i =0;i<flags.length;i++)
        points[flags[i]]++;

    lcd.print("                       ");
    lcd.cursor(1,0);

    lcd.print("R: " + points[0] + " B: " + points[1] + " G: " + points[2]);

}

function writeEvent(text){
    lcd.cursor(0,0);
    lcd.print("                       ");
    lcd.cursor(0,0);
    lcd.print(text);
}